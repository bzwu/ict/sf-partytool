class Partytool {
    constructor(translations) {
        this.translations = translations;

        this.listeners = [
            new ResponseListener(this),
            new ResponseRemoveListener(this),
            new DeletePartyListener(this)
        ];

        this.initListener();
    }

    initListener() {
        this.listeners.forEach(function (listener) {
            $(listener.getElement()).on(listener.getEventName(), listener.execute);
        });
    }

    renderConfirmation(title, text, callback) {
        let alert,
            options = {
                title: title,
                text: text,
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: 'Cancel',
                        value: false,
                        visible: true,
                        closeModal: true
                    },
                    confirm: {
                        text: 'Yes',
                        value: true,
                        closeModal: false
                    }
                }
            };

        alert = swal(options);

        if (callback !== null) {
            alert.then(callback);
        }
    }
}
