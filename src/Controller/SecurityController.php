<?php
declare(strict_types=1);

namespace App\Controller;

use App\Form\LoginForm;
use App\Form\RegisterForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


/**
 * Class SecurityController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 * @Route("/{_locale}", name="security_")
 */
class SecurityController extends Controller
{
    use TargetPathTrait;

    /**
     * @param Request                       $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     *
     * @param AuthenticationUtils           $authenticationUtils
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthorizationCheckerInterface $authorizationChecker, AuthenticationUtils $authenticationUtils)
    {
        if ($authorizationChecker->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException('You\'re already logged in.');
        }

        $error               = $authenticationUtils->getLastAuthenticationError();
        $lastUsername        = $authenticationUtils->getLastUsername();
        $loginForm           = $this->createForm(LoginForm::class, [
            '_username' => $lastUsername
        ]);

        return $this->render('security/login.html.twig', [
            'login_form' => $loginForm->createView(),
            'error'      => $error
        ]);
    }

    /**
     * @param Request $request
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {

    }
}