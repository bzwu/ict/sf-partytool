<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class User
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"username"}, message="user.already_exists")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $lastname;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @var bool
     */
    private $isAdmin = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Response", mappedBy="user")
     *
     * @var Response[]|ArrayCollection
     */
    private $responses;

    /**
     * @Assert\NotBlank(groups={"Registration", "CreateUser"})
     * @Assert\Length(min=8)
     * @var string
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="json_array")
     *
     * @var array
     */
    private $roles = [];

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->responses = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }

    /**
     * Returns whether the user has already
     * responded to the given party or not
     *
     * @param Party $party
     *
     * @return bool
     */
    public function hasRespondedToParty(Party $party): bool
    {
        return !$this->responses->filter(function ($response) use ($party) {
            /** @var Response $response */
            return $response->getParty()->getId() === $party->getId();
        })->isEmpty();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname(string $firstname): User
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname(string $lastname): User
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @param bool $isAdmin
     *
     * @return User
     */
    public function setIsAdmin(bool $isAdmin): User
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * @return Response[]|ArrayCollection|null
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param Response[]|ArrayCollection $responses
     *
     * @return User
     */
    public function setResponses($responses)
    {
        $this->responses = $responses;

        return $this;
    }

    public function getRoles(): ?array
    {
        if (!in_array('ROLE_USER', $this->roles)) {
            $this->roles[] = 'ROLE_USER';
        }

        return $this->roles;
    }

    public function addRole(string $role): User
    {
        $this->roles = array_merge($this->roles, [$role]);

        return $this;
    }

    public function addRoles(array $roles): User
    {
        $this->roles = array_merge($this->roles, $roles);

        return $this;
    }

    public function setRoles(array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        $this->password      = null;

        return $this;
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
