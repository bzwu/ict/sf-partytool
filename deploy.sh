#!/bin/bash

BUILD_DIR=./build
ENV_FILE=${BUILD_DIR}/.env
EXCLUDE_FILE=deploy.exclude

# delete build dir if exists
if [ -d ${BUILD_DIR} ]; then
	echo "deleting old build..."
	rm -rf ${BUILD_DIR}
fi

# create it again
if [ ! -d ${BUILD_DIR} ]; then
	echo "creating new build folder..."
	mkdir ${BUILD_DIR}
fi

# copy stuff to build directory
echo "copying files to build folder... "
rsync -av ./ ${BUILD_DIR} --exclude=${BUILD_DIR}

# remove old env file
if [ -f ${ENV_FILE} ]; then
	echo "removing old .env file..."
	rm ${ENV_FILE}
fi

# Build that stuff
echo "build .env file for production..."
cp ./.env.prod.dist ${ENV_FILE}
echo "installing composer dependencies..."
composer install --working-dir=${BUILD_DIR} --no-dev --ignore-platform-reqs
echo "building static files..."
cd ${BUILD_DIR} && npm install && gulp build && rm -rf ./var/cache/* && rm -rf ./var/log/*

# Deploy!
echo $(pwd)
echo "deploying to production..."
rsync -avz --delete --exclude-from=./${EXCLUDE_FILE} -e "ssh -i ~/.ssh/id_rsa" ./ root@new.scrummer.de:/docker/partytool/data/php/
