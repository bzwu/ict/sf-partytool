<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Party
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\PartyRepository")
 * @ORM\Table(name="party")
 */
class Party
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $startsAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Response", mappedBy="party")
     *
     * @var Response[]|ArrayCollection
     */
    private $responses;

    /**
     * Party constructor.
     */
    public function __construct() { }

    public function __toString(): string
    {
        return (string)$this->id;
    }

    /**
     * Returns whether a given user has accepted the party or not
     *
     * @param User $user
     *
     * @return bool
     */
    public function hasAccepted(User $user): bool
    {
        return !$this->responses->filter(function ($response) use ($user) {
            /** @var Response $response */
            return $response->getUser()->getId() === $user->getId() && $response->isAccepted();
        })->isEmpty();
    }

    public function isUpToDate(): bool
    {
        return $this->startsAt->getTimestamp() >= (new \DateTime())->getTimestamp();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Party
     */
    public function setName(string $name): Party
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Party
     */
    public function setDescription(string $description): Party
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartsAt(): ?\DateTime
    {
        return $this->startsAt;
    }

    /**
     * @param \DateTime $startsAt
     * @return Party
     */
    public function setStartsAt(\DateTime $startsAt): Party
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    /**
     * @return Response[]|ArrayCollection|null
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param Response[]|ArrayCollection $responses
     * @return Party
     */
    public function setResponses($responses)
    {
        $this->responses = $responses;

        return $this;
    }
}
