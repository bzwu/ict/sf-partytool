<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180523133513 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE party DROP responses');
        $this->addSql('ALTER TABLE response ADD party_id INT DEFAULT NULL, ADD user_id INT DEFAULT NULL, DROP party, DROP user');
        $this->addSql('ALTER TABLE response ADD CONSTRAINT FK_3E7B0BFB213C1059 FOREIGN KEY (party_id) REFERENCES party (id)');
        $this->addSql('ALTER TABLE response ADD CONSTRAINT FK_3E7B0BFBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3E7B0BFB213C1059 ON response (party_id)');
        $this->addSql('CREATE INDEX IDX_3E7B0BFBA76ED395 ON response (user_id)');
        $this->addSql('ALTER TABLE user DROP responses');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE party ADD responses VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE response DROP FOREIGN KEY FK_3E7B0BFB213C1059');
        $this->addSql('ALTER TABLE response DROP FOREIGN KEY FK_3E7B0BFBA76ED395');
        $this->addSql('DROP INDEX IDX_3E7B0BFB213C1059 ON response');
        $this->addSql('DROP INDEX IDX_3E7B0BFBA76ED395 ON response');
        $this->addSql('ALTER TABLE response ADD party VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD user VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP party_id, DROP user_id');
        $this->addSql('ALTER TABLE user ADD responses VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
