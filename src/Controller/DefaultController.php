<?php
declare(strict_types=1);

namespace App\Controller;


use App\Entity\Party;
use App\Entity\Response;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DefaultController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 * @Route("/{_locale}", name="default_")
 */
class DefaultController extends Controller
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DefaultController constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="index")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $partyRepo = $em->getRepository(Party::class);
        $parties   = $partyRepo->findAllCurrent();

        return $this->render('default/index.html.twig', [
            'parties' => $parties
        ]);
    }

    /**
     * @Route("/sign-up/{party}/{isAccepted}", name="sign_up")
     * @param Party $party
     *
     * @param bool  $isAccepted
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_CAN_PARTY_SIGNUP')")
     *
     */
    public function signUp(Party $party, bool $isAccepted)
    {
        if (!$party->isUpToDate()) {
            return $this->json(['message' => $this->translator->trans('default.sign_up.party_outdated')], HttpResponse::HTTP_BAD_REQUEST);
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($user->hasRespondedToParty($party)) {
            return $this->json(['message' => $this->translator->trans('default.sign_up.party_already_responded')], HttpResponse::HTTP_BAD_REQUEST);
        }

        $response = new Response();
        $em       = $this->getDoctrine()->getManager();

        $response
            ->setUser($user)
            ->setParty($party)
            ->setIsAccepted($isAccepted);
        $em->persist($response);
        $em->flush();

        return $this->json(['message' => 'success'], HttpResponse::HTTP_OK);
    }
}
