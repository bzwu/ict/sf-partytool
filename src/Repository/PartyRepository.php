<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class PartyRepository
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Repository
 */
class PartyRepository extends EntityRepository
{
    public function findAllCurrent()
    {
        $queryBuilder = $this
            ->createQueryBuilder('party')
            ->andWhere('party.startsAt >= :currentTime');

        $queryBuilder->setParameter('currentTime', (new \DateTime())->format('Y-m-d H:i:s'));

        return $queryBuilder->getQuery()->execute();
    }
}