<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Response
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\ResponseRepository")
 * @ORM\Table(name="response")
 */
class Response
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Party", inversedBy="responses")
     * @ORM\JoinColumn(name="party_id", referencedColumnName="id")
     *
     * @var Party
     */
    private $party;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="responses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $isAccepted;

    /**
     * Response constructor.
     */
    public function __construct() { }

    public function __toString(): string
    {
        return (string)$this->id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Party
     */
    public function getParty(): ?Party
    {
        return $this->party;
    }

    /**
     * @param Party $party
     * @return Response
     */
    public function setParty(Party $party): Response
    {
        $this->party = $party;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Response
     */
    public function setUser(User $user): Response
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->isAccepted;
    }

    /**
     * @param bool $isAccepted
     * @return Response
     */
    public function setIsAccepted(bool $isAccepted): Response
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }
}
