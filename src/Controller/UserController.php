<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterForm;
use App\Form\ResetPasswordForm;
use App\Security\LoginFormAuthenticator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Util\TargetPathTrait;


/**
 * Class UserController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 * @Route("/{_locale}", name="user_")
 */
class UserController extends Controller
{
    use TargetPathTrait;

    /**
     * @Route("/register", name="register")
     * @param Request                       $request
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     *
     * @param GuardAuthenticatorHandler     $authenticatorHandler
     *
     * @param LoginFormAuthenticator        $authenticator
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request, AuthorizationCheckerInterface $authorizationChecker, GuardAuthenticatorHandler $authenticatorHandler, LoginFormAuthenticator $authenticator)
    {
        if ($authorizationChecker->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException('You\'re already logged in.');
        }

        $registerForm = $this->createForm(RegisterForm::class);
        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            /** @var User $user */
            $user = $registerForm->getData();
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $authenticatorHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main'
            );
        }

        return $this->render('user/register.html.twig', [
            'register_form' => $registerForm->createView()
        ]);
    }

    /**
     * @Route("/reset-password", name="reset_password")
     */
    public function resetPassword(Request $request)
    {
        $form = $this->createForm(ResetPasswordForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $oldPassword = $data['password'];
            $newPassword = $data['plainPassword'];
            /** @var User $user */
            $user = $this->getUser();

            if (password_verify($oldPassword, $user->getPassword())) {
                $em = $this->getDoctrine()->getManager();

                $user->setPlainPassword($newPassword);
                $em->persist($user);
                $em->flush();
            } else {
                $this->addFlash('error', 'Dein altes Passwort ist nicht korrekt');
            }
        }

        return $this->render('user/reset_password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}