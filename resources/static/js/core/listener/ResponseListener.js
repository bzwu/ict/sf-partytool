class ResponseListener extends AbstractListener {
    getElement() {
        return '.js-response-listener';
    }

    getEventName() {
        return 'click';
    }

    execute(event) {
        let $this = $(this),
            postUrl = $this.data('url'),
            request;

        ResponseListener.app.renderConfirmation('Confirm', ResponseListener.app.translations['js.sign_up.confirm'], (confirmed) => {
            if (!confirmed) {
                return;
            }

            request = $.post(postUrl);

            request.done((data) => {
                console.log(data);
                swal('done').then(() => {
                    window.location.reload();
                });
            });
            request.fail((data) => {
                console.log(data);
                swal('fail').then(() => {
                    window.location.reload();
                });
            });
        });
    }
}
