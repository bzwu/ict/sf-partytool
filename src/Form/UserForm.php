<?php
declare(strict_types=1);


namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class UserForm
 *
 * @author Scrummer<scrummer@gmx.ch>
 * @package App\Form
 */
class UserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'admin.user.username'
            ])
            ->add('firstname', null, [
                'label' => 'admin.user.firstname'
            ])
            ->add('lastname', null, [
                'label' => 'admin.user.lastname'
            ])
            ->add('isAdmin', CheckboxType::class, [
                'label' => 'admin.user.is_admin'
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'admin.user.roles',
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'admin.user.role_user' => 'ROLE_USER',
                    'admin.user.role_admin' => 'ROLE_ADMIN'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'admin.user.password'
                ],
                'second_options' => [
                    'label' => 'admin.user.repeat_password'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['Default']
        ]);
    }

    public function getBlockPrefix()
    {
        return 'edit_user';
    }
}
