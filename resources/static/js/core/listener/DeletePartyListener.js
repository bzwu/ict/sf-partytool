class DeletePartyListener extends AbstractListener {
    getElement() {
        return '.js-party-delete';
    }

    getEventName() {
        return 'click';
    }

    execute(event) {
        let $this = $(this),
            postUrl = $this.attr('href'),
            request;

        event.preventDefault();

        DeletePartyListener.app.renderConfirmation(
            'Confirm',
            DeletePartyListener.app.translations['js.party.confirm_remove'],
            (confirmed) => {
                if (!confirmed) {
                    return;
                }

                request = $.post(postUrl);

                request.done((data) => {
                    console.log(data);
                    swal('done').then(() => {
                        window.location.reload();
                    });
                });
                request.fail((data) => {
                    console.log(data);
                    swal('fail').then(() => {
                        window.location.reload();
                    });
                });
            }
        );
    }
}
