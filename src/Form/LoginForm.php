<?php
declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class LoginForm
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Type
 */
class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', null, [
                'label' => 'security.login.username'
            ])
            ->add('_password', PasswordType::class, [
                'label' => 'security.login.password'
            ]);
    }
}