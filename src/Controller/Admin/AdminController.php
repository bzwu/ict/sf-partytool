<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Party;
use App\Entity\User;
use App\Form\UserForm;
use App\Form\PartyForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 *
 * @package App\Controller
 *
 * @Route("/{_locale}/admin", name="admin_")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @Route("/party/list", name="party_list")
     * @param Request $request
     *
     * @return Response
     */
    public function listParties(Request $request)
    {
        $em        = $this->getDoctrine()->getManager();
        $partyRepo = $em->getRepository(Party::class);
        $parties   = $partyRepo->findAll();

        return $this->render('admin/list_parties.html.twig', [
            'parties' => $parties
        ]);
    }

    /**
     * @Route("/party/edit/{party}", name="party_edit")
     * @param Request $request
     * @param Party   $party
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editParty(Request $request, Party $party)
    {
        $form = $this->createForm(PartyForm::class, $party);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $party = $form->getData();

            $em->persist($party);
            $em->flush();

            return $this->redirectToRoute('admin_party_list');
        }

        return $this->render('admin/add_party.html.twig', [
            'party_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/party/delete/{party}", methods={"POST"}, name="party_delete")
     * @param Request $request
     * @param Party   $party
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteParty(Request $request, Party $party)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($party);
        $em->flush();


        return $this->json(['message' => 'success']);
    }

    /**
     * @Route("/party/add", name="party_add")
     * @param Request $request
     *
     * @return Response
     */
    public function addParty(Request $request)
    {
        $addPartyForm = $this->createForm(PartyForm::class);
        $addPartyForm->handleRequest($request);

        if ($addPartyForm->isSubmitted() && $addPartyForm->isValid()) {
            /** @var Party $party */
            $party = $addPartyForm->getData();
            $em    = $this->getDoctrine()->getManager();

            $em->persist($party);
            $em->flush();

            $this->addFlash('success', sprintf('Successfully created Party "%s".', $party->getName()));

            return $this->redirectToRoute('admin_party_list');
        }

        return $this->render('admin/add_party.html.twig', [
            'party_form' => $addPartyForm->createView()
        ]);
    }

    /**
     * @Route("/party/responses/{party}", name="party_responses")
     *
     * @param Request $request
     * @param Party   $party
     *
     * @return Response
     */
    public function showResponses(Request $request, Party $party)
    {
        return $this->render('admin/party_responses.html.twig', [
            'party' => $party
        ]);
    }

    /**
     * @Route("/party/responses/delete/{response}", name="response_delete")
     *
     * @param Request              $request
     * @param \App\Entity\Response $response
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteResponse(Request $request, \App\Entity\Response $response)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($response);
        $em->flush();

        return $this->json(['message' => 'success']);
    }

    /**
     * @param Request $request
     * @Route("/users/list", name="users_list")
     *
     * @return Response
     */
    public function listUsers(Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository(User::class);
        $users    = $userRepo->findAll();

        return $this->render('admin/users_list.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/users/add", name="user_add")
     * @param Request $request
     *
     * @return Response
     */
    public function addUser(Request $request)
    {
        $form = $this->createForm(UserForm::class, null, [
            'validation_groups' => ['Default', 'CreateUser']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $form->getData();

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_list');
        }

        return $this->render('admin/user.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/edit/{user}", name="users_edit")
     *
     * @fixme jpache: Password is changed every time.... :(
     *
     * @param Request $request
     * @param User    $user
     *
     * @return Response
     */
    public function editUser(Request $request, User $user)
    {
        $form = $this->createForm(UserForm::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $form->getData();

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_list');
        }

        return $this->render('admin/user.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/users/delete/{user}", name="users_delete")
     *
     * @param Request $request
     * @param User    $user
     */
    public function deleteUser(Request $request, User $user)
    {

    }
}
