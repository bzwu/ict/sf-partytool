<?php
declare(strict_types=1);

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class RoutingController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 * @Route("/", name="routing_")
 */
class RoutingController extends Controller
{
    /**
     * @Route("/", name="route")
     */
    public function route()
    {
        return $this->redirectToRoute('default_index');
    }
}