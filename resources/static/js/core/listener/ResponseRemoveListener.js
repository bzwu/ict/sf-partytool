class ResponseRemoveListener extends AbstractListener {
    getElement() {
        return '.js-response-remove-listener';
    }

    getEventName() {
        return 'click';
    }

    execute(event) {
        let $this = $(this),
            postUrl = $this.data('url'),
            request;

        ResponseRemoveListener.app.renderConfirmation('Confirm', ResponseListener.app.translations['js.response.remove.confirm'], (confirmed) => {
            if (!confirmed) {
                return;
            }

            request = $.post(postUrl);

            request.done((data) => {
                console.log(data);
                swal('done').then(() => {
                    window.location.reload();
                });
            });
            request.fail((data) => {
                console.log(data);
                swal('fail').then(() => {
                    window.location.reload();
                });
            });
        });
    }
}
