<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Party;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class PartyForm
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Type
 */
class PartyForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'admin.party_form.name'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'admin.party_form.description'
            ])
            ->add('startsAt', DateType::class, [
                'label' => 'admin.party_form.starts_at',
                'widget' => 'single_text'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'admin.party_form.btn_save'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Party::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'party';
    }
}