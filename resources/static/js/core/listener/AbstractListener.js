class AbstractListener {
    constructor(app) {
        AbstractListener.app = app;
    }

    getElement() {
        throw new Error('Method "AbstractListener.getElement" is abstract. You have to implement it first.');
    }

    getEventName() {
        throw new Error('Method "AbstractListener.getEventName" is abstract. You have to implement it first.');
    }

    execute() {
        throw new Error('Method "AbstractListener.execute" is abstract. You have to implement it first.');
    }
}